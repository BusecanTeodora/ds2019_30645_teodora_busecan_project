package com.sd.assignment3.model;

public class RecomandationDTO {

    private long activityId;
    private long patientId;
    private String message;

    public RecomandationDTO() {
    }

    public RecomandationDTO(long activityId, long patientId, String message) {
        this.activityId = activityId;
        this.patientId = patientId;
        this.message = message;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
