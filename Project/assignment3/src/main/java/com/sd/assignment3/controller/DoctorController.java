package com.sd.assignment3.controller;

import com.sd.assignment3.model.MedicationStatusLog;
import com.sd.assignment3.service.*;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/assignment4")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class DoctorController {

    private ClientAssignment4ServiceImpl service;

    public DoctorController() {
        URL url = null;
        try {
            url = new URL("http://localhost:2750/employeeservice?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ClientAssignment4ServiceImplService clientService = new ClientAssignment4ServiceImplService(url);
        service = clientService.getClientAssignment4ServiceImplPort();
    }

    @PostMapping(value = "/activities/{patientId}")
    public List<PatientActivity> getAllActivitiesFor(@PathVariable long patientId, @RequestBody LocalDate forDate) {
//        System.out.println("AFSGNSDFAJOJGWDBG");
        System.out.println(service.getActivitiesForPatient(patientId).get(0).getStartTime());
        List<PatientActivity> patientActivities = service.getActivitiesForPatient(patientId);
        List<PatientActivity> goodies = new ArrayList<>();
        for(PatientActivity patientActivity: patientActivities) {
            String firstFourChars = "";
           // System.out.println("AFSGNSDFAJOJGWDBG");
            if (patientActivity.getStartTime().length() > 10)
            {
                firstFourChars = patientActivity.getStartTime().substring(0, 10);
            }
            else
            {
                firstFourChars = patientActivity.getStartTime();
            }
            System.out.println(firstFourChars);
            System.out.println(String.valueOf(forDate));
           if(firstFourChars.equals(String.valueOf(forDate))){
             //  System.out.println("AFSGNSDFAJOJGWDBG");
               goodies.add(patientActivity);
           }
            System.out.println(goodies.size());
            }
       // System.out.println("AFSGNSDFAJOJGWDBG");


        return goodies;
    }

    @PostMapping(value = "/medicationLog/{patientId}")
    public List<MedicationStatusLog> getMedicationLogFor(@PathVariable long patientId, @RequestBody LocalDate forDate) {
        //System.out.println(patientId);
        List<MedicationStatusLogDTO> medicationStatusLogDTOS = service.getMedicationStatusForPatient(patientId);
        System.out.println(medicationStatusLogDTOS.size());
        List<MedicationStatusLog> goodies = new ArrayList<>();

        for(MedicationStatusLogDTO medicationStatusLogDTO: medicationStatusLogDTOS) {
            String firstFourChars = "";
//             System.out.println("AFSGNSDFAJOJGWDBG");

                System.out.println(medicationStatusLogDTO.getDate());
                System.out.println(medicationStatusLogDTO.getId());
                System.out.println(medicationStatusLogDTO.getMessage());
                System.out.println(medicationStatusLogDTO.getPatientId());

            if (medicationStatusLogDTO.getDate().length() > 10)
            {
                firstFourChars = medicationStatusLogDTO.getDate().substring(0, 10);
            }
            else
            {
                firstFourChars = medicationStatusLogDTO.getDate();
            }
            System.out.println(firstFourChars);
            System.out.println(String.valueOf(forDate));
            if(firstFourChars.contentEquals(String.valueOf(forDate))){
                  System.out.println("AFSGNSDFAJOJGWDBG");
               MedicationStatusLog medicationStatusLog = new MedicationStatusLog(medicationStatusLogDTO.getId(),medicationStatusLogDTO.getMessage(),medicationStatusLogDTO.getPatientId(), LocalDateTime.parse(medicationStatusLogDTO.getDate()));
            goodies.add(medicationStatusLog);
            }
            System.out.println(goodies.size());
        }
         //System.out.println("AFSGNSDFAJOJGWDBG");


        return goodies;
    }

    @RequestMapping(value = "/activities/valid", method = RequestMethod.PUT)
    public void markActivityAsNormal(@RequestBody Integer activityId) {
        service.markActivityAsNormal(activityId);
//        System.out.println(activityId);
    }

    @RequestMapping(value = "/activities/recomandation", method = RequestMethod.POST)
    public void addRecomandation(@RequestBody Recomandation recomandation) {
        service.addRecomandation(recomandation);
//        System.out.println(recomandation.getActivityId() + " " + recomandation.getMessage());
    }

    @RequestMapping(value = "/activities/recomandation/{patientId}", method = RequestMethod.GET)
    public List<Recomandation> getRecomandations(@PathVariable Integer patientId) {
        return service.getRecomandations(patientId);
    }
}
