package com.sd.assignment3.model;

import java.time.LocalDateTime;

public class MedicationStatusLog {

    private long id;
    private String message;
    private long patientId;
    private LocalDateTime date;

    public MedicationStatusLog(long id, String message, long patientId, LocalDateTime date) {
        this.message = message;
        this.id = id;
        this.patientId = patientId;
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public LocalDateTime getDate() { return date; }

    public void setDate(LocalDateTime date) { this.date = date; }
}
