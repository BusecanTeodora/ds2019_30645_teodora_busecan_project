package com.sd.assignment3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3Application {

    public static void main(String[] args) {
//        URL url = null;
//        try {
//            url = new URL("http://localhost:2750/employeeservice?wsdl");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        ClientAssignment4ServiceImplService clientService
//                = new ClientAssignment4ServiceImplService(url);
//        ClientAssignment4ServiceImpl employeeServiceProxy
//                = clientService.getClientAssignment4ServiceImplPort();
//
//        List<PatientActivity> activities
//                = employeeServiceProxy.getActivitiesForPatient(33L);
//        System.out.println(activities);
        SpringApplication.run(Assignment3Application.class, args);

    }

}
