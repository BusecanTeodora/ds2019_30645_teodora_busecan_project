
package com.sd.assignment3.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetActivitiesForPatientResponse_QNAME = new QName("http://service.assignment1.SD/", "getActivitiesForPatientResponse");
    private final static QName _AddRecomandationResponse_QNAME = new QName("http://service.assignment1.SD/", "addRecomandationResponse");
    private final static QName _GetRecomandations_QNAME = new QName("http://service.assignment1.SD/", "getRecomandations");
    private final static QName _GetRecomandationsResponse_QNAME = new QName("http://service.assignment1.SD/", "getRecomandationsResponse");
    private final static QName _GetActivitiesForPatient_QNAME = new QName("http://service.assignment1.SD/", "getActivitiesForPatient");
    private final static QName _GetMedicationStatusForPatient_QNAME = new QName("http://service.assignment1.SD/", "getMedicationStatusForPatient");
    private final static QName _MarkActivityAsNormal_QNAME = new QName("http://service.assignment1.SD/", "markActivityAsNormal");
    private final static QName _AddRecomandation_QNAME = new QName("http://service.assignment1.SD/", "addRecomandation");
    private final static QName _MarkActivityAsNormalResponse_QNAME = new QName("http://service.assignment1.SD/", "markActivityAsNormalResponse");
    private final static QName _GetMedicationStatusForPatientResponse_QNAME = new QName("http://service.assignment1.SD/", "getMedicationStatusForPatientResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMedicationStatusForPatientResponse }
     * 
     */
    public GetMedicationStatusForPatientResponse createGetMedicationStatusForPatientResponse() {
        return new GetMedicationStatusForPatientResponse();
    }

    /**
     * Create an instance of {@link AddRecomandation }
     * 
     */
    public AddRecomandation createAddRecomandation() {
        return new AddRecomandation();
    }

    /**
     * Create an instance of {@link MarkActivityAsNormalResponse }
     * 
     */
    public MarkActivityAsNormalResponse createMarkActivityAsNormalResponse() {
        return new MarkActivityAsNormalResponse();
    }

    /**
     * Create an instance of {@link MarkActivityAsNormal }
     * 
     */
    public MarkActivityAsNormal createMarkActivityAsNormal() {
        return new MarkActivityAsNormal();
    }

    /**
     * Create an instance of {@link GetActivitiesForPatient }
     * 
     */
    public GetActivitiesForPatient createGetActivitiesForPatient() {
        return new GetActivitiesForPatient();
    }

    /**
     * Create an instance of {@link GetMedicationStatusForPatient }
     * 
     */
    public GetMedicationStatusForPatient createGetMedicationStatusForPatient() {
        return new GetMedicationStatusForPatient();
    }

    /**
     * Create an instance of {@link AddRecomandationResponse }
     * 
     */
    public AddRecomandationResponse createAddRecomandationResponse() {
        return new AddRecomandationResponse();
    }

    /**
     * Create an instance of {@link GetRecomandations }
     * 
     */
    public GetRecomandations createGetRecomandations() {
        return new GetRecomandations();
    }

    /**
     * Create an instance of {@link GetRecomandationsResponse }
     * 
     */
    public GetRecomandationsResponse createGetRecomandationsResponse() {
        return new GetRecomandationsResponse();
    }

    /**
     * Create an instance of {@link GetActivitiesForPatientResponse }
     * 
     */
    public GetActivitiesForPatientResponse createGetActivitiesForPatientResponse() {
        return new GetActivitiesForPatientResponse();
    }

    /**
     * Create an instance of {@link Recomandation }
     * 
     */
    public Recomandation createRecomandation() {
        return new Recomandation();
    }

    /**
     * Create an instance of {@link PatientActivity }
     * 
     */
    public PatientActivity createPatientActivity() {
        return new PatientActivity();
    }

    /**
     * Create an instance of {@link MedicationStatusLogDTO }
     * 
     */
    public MedicationStatusLogDTO createMedicationStatusLogDTO() {
        return new MedicationStatusLogDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivitiesForPatientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getActivitiesForPatientResponse")
    public JAXBElement<GetActivitiesForPatientResponse> createGetActivitiesForPatientResponse(GetActivitiesForPatientResponse value) {
        return new JAXBElement<GetActivitiesForPatientResponse>(_GetActivitiesForPatientResponse_QNAME, GetActivitiesForPatientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRecomandationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "addRecomandationResponse")
    public JAXBElement<AddRecomandationResponse> createAddRecomandationResponse(AddRecomandationResponse value) {
        return new JAXBElement<AddRecomandationResponse>(_AddRecomandationResponse_QNAME, AddRecomandationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRecomandations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getRecomandations")
    public JAXBElement<GetRecomandations> createGetRecomandations(GetRecomandations value) {
        return new JAXBElement<GetRecomandations>(_GetRecomandations_QNAME, GetRecomandations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRecomandationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getRecomandationsResponse")
    public JAXBElement<GetRecomandationsResponse> createGetRecomandationsResponse(GetRecomandationsResponse value) {
        return new JAXBElement<GetRecomandationsResponse>(_GetRecomandationsResponse_QNAME, GetRecomandationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivitiesForPatient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getActivitiesForPatient")
    public JAXBElement<GetActivitiesForPatient> createGetActivitiesForPatient(GetActivitiesForPatient value) {
        return new JAXBElement<GetActivitiesForPatient>(_GetActivitiesForPatient_QNAME, GetActivitiesForPatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMedicationStatusForPatient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getMedicationStatusForPatient")
    public JAXBElement<GetMedicationStatusForPatient> createGetMedicationStatusForPatient(GetMedicationStatusForPatient value) {
        return new JAXBElement<GetMedicationStatusForPatient>(_GetMedicationStatusForPatient_QNAME, GetMedicationStatusForPatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MarkActivityAsNormal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "markActivityAsNormal")
    public JAXBElement<MarkActivityAsNormal> createMarkActivityAsNormal(MarkActivityAsNormal value) {
        return new JAXBElement<MarkActivityAsNormal>(_MarkActivityAsNormal_QNAME, MarkActivityAsNormal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRecomandation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "addRecomandation")
    public JAXBElement<AddRecomandation> createAddRecomandation(AddRecomandation value) {
        return new JAXBElement<AddRecomandation>(_AddRecomandation_QNAME, AddRecomandation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MarkActivityAsNormalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "markActivityAsNormalResponse")
    public JAXBElement<MarkActivityAsNormalResponse> createMarkActivityAsNormalResponse(MarkActivityAsNormalResponse value) {
        return new JAXBElement<MarkActivityAsNormalResponse>(_MarkActivityAsNormalResponse_QNAME, MarkActivityAsNormalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMedicationStatusForPatientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.assignment1.SD/", name = "getMedicationStatusForPatientResponse")
    public JAXBElement<GetMedicationStatusForPatientResponse> createGetMedicationStatusForPatientResponse(GetMedicationStatusForPatientResponse value) {
        return new JAXBElement<GetMedicationStatusForPatientResponse>(_GetMedicationStatusForPatientResponse_QNAME, GetMedicationStatusForPatientResponse.class, null, value);
    }

}
