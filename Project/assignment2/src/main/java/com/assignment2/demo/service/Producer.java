package com.assignment2.demo.service;

import com.assignment2.demo.model.MonitoredData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class Producer {

    //    @Autowired
//    private AmqpTemplate amqpTemplate;
//
//    @Value("${jsa.rabbitmq.exchange}")
//    private String exchange;
//
//    @Value("${jsa.rabbitmq.routingkey}")
//    private String routingkey;
//
//    public void produce(MonitoredData msg){
//        amqpTemplate.convertAndSend(exchange, routingkey, msg);
//        System.out.println("Send msg = " + msg);
//    }
    private final static String QUEUE_NAME="activities";

     public Producer() {
         try {
             ConnectionFactory factory = new ConnectionFactory();
             factory.setHost("localhost");
             objectMapper=new ObjectMapper();
             objectMapper.registerModule(new JavaTimeModule());
             Connection connection = factory.newConnection();
             channel = connection.createChannel();
             channel.queueDeclare(QUEUE_NAME, false, false, false, null);

         } catch (Exception e) {
             e.printStackTrace();
         }
     }

     private Channel channel;
     private ObjectMapper objectMapper;

     public void produce(MonitoredData activity) {
         try {
             String message = objectMapper.writeValueAsString(activity);
             channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
             System.out.println("Sent message--: " + message);
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
}
