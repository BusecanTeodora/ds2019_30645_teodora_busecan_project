package com.assignment2.demo.model;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
public class MonitoredData {

    private Long patientId = 0L;
    private Long startTime;
    private Long endTime;
    private String activity;

    public MonitoredData(){
        activity = "init";
    }

    public MonitoredData(String startTime, String endTime, String activity){
        LocalDateTime startTimeDate = LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.startTime = startTimeDate.toInstant(ZoneOffset.UTC).getEpochSecond();
        LocalDateTime endTimeDate = LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.endTime = endTimeDate.toInstant(ZoneOffset.UTC).getEpochSecond();
        this.activity = activity;
    }

    public MonitoredData(Long patientId, String startTime, String endTime, String activity){
        LocalDateTime startTimeDate = LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.startTime = startTimeDate.toInstant(ZoneOffset.UTC).getEpochSecond();
        LocalDateTime endTimeDate = LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.endTime = endTimeDate.toInstant(ZoneOffset.UTC).getEpochSecond();
        this.activity = activity;
    }

    public Long getStartTime(){
        return startTime;
    }
//
    public Long getEndTime(){
        return endTime;
    }

    public String getActivity(){
        return activity;
    }

    public void setStartTime(Long startTime){
        this.startTime = startTime;
    }
//
    public void setEndTime(Long endTime){
        this.endTime = endTime;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public String toString(){
        return "id: " + patientId + " start: " + startTime + ", end: "+ endTime+ ", activity: "+activity;
    }
}
