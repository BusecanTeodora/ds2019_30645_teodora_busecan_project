package com.assignment2.demo.service;

import com.assignment2.demo.model.MonitoredData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class ActivityService {

//    @Autowired
    Producer producer;

    private List<MonitoredData> monitoredData;
    private int index;

    public ActivityService() {
        producer = new Producer();
        index = 0;
    }

    public List<MonitoredData> getMonitoredData() {
        return monitoredData;
    }

    public void setMonitoredData(List<MonitoredData> monitoredData) {
        this.monitoredData = monitoredData;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @RequestMapping("/send")
    public String sendMsg(@RequestParam("msg")MonitoredData msg){
        producer.produce(msg);
        return "Done";
    }

    @Scheduled(initialDelay = 2000, fixedRate = 2000)
    public void startSending() {
        if(index < monitoredData.size()) {
            sendMsg(monitoredData.get(index));
            index++;
        }
    }
}
