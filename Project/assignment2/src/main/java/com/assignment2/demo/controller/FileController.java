package com.assignment2.demo.controller;

import com.assignment2.demo.model.MonitoredData;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class FileController {

    public FileController() { }

    public Stream<String> readFile(String fileName){
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public List<MonitoredData> convertToList(Stream<String> stream){
        List<MonitoredData> data = stream.map(line ->{
            String[] splitLine = line.split("\t\t");
            System.out.println(line);
            MonitoredData md = new MonitoredData(splitLine[0], splitLine[1], splitLine[2]);
            return md;
        }).collect(Collectors.toList());
        return data;
    }
}
