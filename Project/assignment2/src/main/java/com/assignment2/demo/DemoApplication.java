package com.assignment2.demo;

import com.assignment2.demo.controller.FileController;
import com.assignment2.demo.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.stream.Stream;

@SpringBootApplication
@EnableScheduling
public class DemoApplication {

    @Autowired
    ActivityService activityService;
    @Autowired
    FileController fileController;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public void startSending() {
        Stream<String> fileStream = fileController.readFile("C:\\Users\\DeLL\\Desktop\\mareeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\\assignment2\\src\\main\\resources\\activity.txt");
        activityService.setMonitoredData(fileController.convertToList(fileStream));
    }

}
