package com.example.springdemo.services;

import com.example.springdemo.dto.ActivityDTOTEMA4;
import com.example.springdemo.dto.MedicationStatusLogDTO;
import com.example.springdemo.entities.Activities;
import com.example.springdemo.entities.MedicationStatusLog;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.Recomandations;
import com.example.springdemo.repositories.ActivitiesRepository;
import com.example.springdemo.repositories.DrugStatusRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.RecomandationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@WebService
public class ClientAssignment4ServiceImpl implements A4Service {

    @Autowired
    ActivitiesRepository activitiesRepository;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    DrugStatusRepository drugStatusRepository;
    @Autowired
    RecomandationRepository recomandationRepository;
    @Autowired
    ActivitiesService activitiesService;

    public ClientAssignment4ServiceImpl() {
        System.out.println("created");
    }

    @Override
    @WebMethod(operationName = "getActivitiesForPatient")
    public List<ActivityDTOTEMA4> getActivitiesForPatient(@WebParam(name = "patientId") Integer patientId) {
        List<Activities> activities = activitiesRepository.findAll();
        List<Activities> goodOnes = new ArrayList<>();
        for(Activities activities1: activities){
            if (activities1.getPatient().getIdpatient().equals(patientId)) {
                goodOnes.add(activities1);
            }
        }
     //   List<Activities> monitoredData = activitiesRepository.findAllByIdpatient(patientId);
        if(goodOnes.isEmpty()) {
            return new ArrayList<>();
        }
        List<ActivityDTOTEMA4> activityDTOTEMA4s = new ArrayList<>();
        activityDTOTEMA4s = goodOnes.stream().map(data -> {
            Long interval = activitiesService.compareTwoTimeStampsInMin(data.getStart_time(), data.getEnd_time());
            return new ActivityDTOTEMA4(data.getIdactivities(), interval, data.getActivity(), data.getStart_time().toString(), data.isValid());
        }).collect(Collectors.toList());
        return activityDTOTEMA4s;
    }

    @Override
    public List<MedicationStatusLogDTO> getMedicationStatusForPatient(int patientId) {
        List<Patient> patients = patientRepository.findAll();
        System.out.println(patientId);
       // Integer patientId = null;
//        for(Patient p:patients){
//            if(p.getUser().getIduser().equals(userid)){
//                patientId = p.getIdpatient();
//            }
//        }
        List<MedicationStatusLog> medicationStatusLogs = drugStatusRepository.findAll();

        List<MedicationStatusLog> goodies = new ArrayList<>();
        for(MedicationStatusLog medicationStatusLog :medicationStatusLogs){
            System.out.println(String.valueOf(medicationStatusLog.getPatientId()));
            System.out.println(patientId);
            System.out.println(String.valueOf(medicationStatusLog.getPatientId()).equals(String.valueOf(patientId)));
            if(String.valueOf(medicationStatusLog.getPatientId()).equals(String.valueOf(patientId))){
                goodies.add(medicationStatusLog);
            }
        }

        System.out.println(goodies.size());
        if(!goodies.isEmpty()) {
           // System.out.println("NU VREAU? Aci");
       //     return goodies.stream().map(data -> new MedicationStatusLogDTO( data.getMessage(), data.getPatientId())).collect(Collectors.toList());
            return goodies.stream().map(data -> new MedicationStatusLogDTO(data.getId(), data.getMessage(), data.getPatientId(), data.getDate().toString())).collect(Collectors.toList());

        }
       // System.out.println(goodies.size());
        return new ArrayList<>();
    }

    @Override
    public void markActivityAsNormal(Integer activityId) {
        Activities activity = activitiesRepository.findById(activityId).get();
        activity.setValid(true);
        activitiesRepository.save(activity);
    }

    @Override
    public void addRecomandation(Recomandations recomandation) {
        recomandationRepository.save(recomandation);
    }

    @Override
    public List<Recomandations> getRecomandations(Integer forPatientId) {
        System.out.println(recomandationRepository.findAllByPatientId(forPatientId));
        return recomandationRepository.findAllByPatientId(forPatientId);
    }
}

