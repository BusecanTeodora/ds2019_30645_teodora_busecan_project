package com.example.springdemo.services;

import com.example.springdemo.dto.ActivityDTOTEMA4;
import com.example.springdemo.dto.MedicationStatusLogDTO;
import com.example.springdemo.entities.Recomandations;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import java.util.List;

@Service
public interface A4Service {

    @WebMethod
    List<ActivityDTOTEMA4> getActivitiesForPatient(Integer patientId);

    @WebMethod
    List<MedicationStatusLogDTO> getMedicationStatusForPatient(int patientId);

    @WebMethod
    void markActivityAsNormal(Integer activityId);

    @WebMethod
    void addRecomandation(Recomandations recomandation);

    @WebMethod
    List<Recomandations> getRecomandations(Integer forPatientId);

}
//wsimport -keep -p service -d D:\Faculta\IV\ass3\ds2019_30245_serban_coroiu_assignment_3\assignment3\src\main\java\com\sd\assignment3 http://localhost:2750/employeeservice?wsdl
