package com.example.springdemo.controller;


import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.dto.IntakeDTOView;
import com.example.springdemo.dto.PatientDTOinsert;
import com.example.springdemo.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.PatientService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/intake")
public class IntakeController {

    private final IntakeService intakeService;

    @Autowired
    public IntakeController(IntakeService intakeService) {
        this.intakeService= intakeService;
    }

    @PostMapping()
    public List<IntakeDTOView> findAll(@RequestBody Integer idpatient){
        return intakeService.findAll(idpatient);
    }

    @PostMapping(value = "/insert")
    public void insertIntake(@RequestBody IntakeDTOView intakeDTOView){
         intakeService.insert(intakeDTOView);
    }


}
