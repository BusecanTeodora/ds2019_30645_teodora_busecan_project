package com.example.springdemo.repositories;

import com.example.springdemo.entities.MedicationStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DrugStatusRepository extends JpaRepository<MedicationStatusLog, Long> {
    List<MedicationStatusLog> findAllByPatientId(long patientId);

}
