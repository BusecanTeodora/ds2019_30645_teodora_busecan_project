package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregiver")
public class
Caregiver implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idcaregiver", unique = true, nullable = false)
    private Integer idcaregiver;

    @ManyToOne
    @JoinColumn(name = "iddoctor")
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "birthday")
    private String date;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    private String gender;

    @OneToMany(mappedBy = "caregiver", cascade = CascadeType.ALL)
    private List<Patient> patientList;

    public Caregiver() {
    }

    public Caregiver(Integer idcaregiver, String name, String date, String address, String gender) {
        this.idcaregiver = idcaregiver;
        this.name = name;
        this.date = date;
        this.address = address;
        this.gender = gender;
    }

    public Integer getIdcaregiver() {
        return idcaregiver;
    }

    public void setIdcaregiver(Integer idcaregiver) {
        this.idcaregiver = idcaregiver;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }
}
