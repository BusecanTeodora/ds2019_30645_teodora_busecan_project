package com.example.springdemo.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "drug_status")
public class MedicationStatusLog {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "message")
    private String message;

    @Column(name = "patient_id")
    private long patientId;

    @Column(name = "date")
    private LocalDateTime date;

    public MedicationStatusLog(String message, long patientId, LocalDateTime date) {
        this.message = message;
        this.patientId = patientId;
        this.date = date;
    }

    public MedicationStatusLog(long id, String message, long patientId, LocalDateTime date) {
        this.id = id;
        this.message = message;
        this.patientId = patientId;
        this.date = date;
    }

    public MedicationStatusLog() { }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public LocalDateTime getDate() { return date; }

    public void setDate(LocalDateTime date) { this.date = date; }
}
