package com.example.springdemo.repositories;

import com.example.springdemo.entities.Recomandations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecomandationRepository extends JpaRepository<Recomandations, Integer> {
    List<Recomandations> findAllByPatientId(Integer patientId);
}
