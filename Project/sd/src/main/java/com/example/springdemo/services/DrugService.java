package com.example.springdemo.services;

import com.example.springdemo.dto.DrugDTO;
import com.example.springdemo.dto.builders.DrugBuilder;
import com.example.springdemo.entities.Drug;
import com.example.springdemo.entities.IntakeDrugs;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DrugRepository;
import com.example.springdemo.repositories.IntakeDrugsRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DrugService {

    private final DrugRepository drugRepository;
    private final IntakeDrugsRepository intakeDrugsRepository;

    @Autowired
    public DrugService(DrugRepository drugRepository, IntakeDrugsRepository intakeDrugsRepository) {
        this.drugRepository = drugRepository;
        this.intakeDrugsRepository = intakeDrugsRepository;
    }

    public DrugDTO finddrugById(Integer id) {
        Optional<Drug> drug = drugRepository.findById(id);

        if (!drug.isPresent()) {
            throw new ResourceNotFoundException("Druh", "drug id", id);
        }
        return DrugBuilder.generateDTOFromEntity(drug.get());
    }

    public List<DrugDTO> findAll() {
        List<Drug> drugs = drugRepository.findAll();

        return drugs.stream().map(DrugBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public Integer insert(DrugDTO drugDTO) {
        PersonFieldValidator.validateInsertOrUpdate(drugDTO);

        Drug drug = drugRepository.findByName(drugDTO.getName());
        if (drug != null) {
            System.out.println("FAIL");
        }

        return drugRepository.save(DrugBuilder.generateEntityFromDTO(drugDTO))
                .getIddrug();
    }

    public Integer update(DrugDTO drugDTO) {

        Optional<Drug> drug = drugRepository.findById(drugDTO.getIddrug());

        if (!drug.isPresent()) {
            throw new ResourceNotFoundException("Drug", "drug id", drugDTO.getIddrug().toString());
        }
        return drugRepository.save(DrugBuilder.generateEntityFromDTO(drugDTO)).getIddrug();
    }

    public void delete(DrugDTO drugDTO) {
        List<IntakeDrugs> intakeDrugs = intakeDrugsRepository.findAll();
        for (IntakeDrugs intakeDrugs1 : intakeDrugs) {
            if (intakeDrugs1.getDrug().getIddrug() == drugDTO.getIddrug()) {
                intakeDrugsRepository.delete(intakeDrugs1);
            }
        }
        this.drugRepository.deleteById(drugDTO.getIddrug());
    }

}
