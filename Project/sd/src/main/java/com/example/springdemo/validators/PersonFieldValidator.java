package com.example.springdemo.validators;

import com.example.springdemo.dto.DrugDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class PersonFieldValidator{

    private static final Log LOGGER = LogFactory.getLog(PersonFieldValidator.class);

    public static void validateInsertOrUpdate(DrugDTO drugDto) {

        List<String> errors = new ArrayList<>();
        if (drugDto == null) {
            errors.add("personDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }


        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }
}
