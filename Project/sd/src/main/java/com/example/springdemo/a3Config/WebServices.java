package com.example.springdemo.a3Config;

import com.example.springdemo.services.ClientAssignment4ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class WebServices{

    @Autowired
    private ClientAssignment4ServiceImpl clientAssignment4Service;

    @Bean
    public void setupClientAssignment3Service() {
        System.out.println("exposing Service for Assignment4");
        System.out.println(clientAssignment4Service);
        Endpoint.publish("http://0.0.0.0:2750/employeeservice",
                clientAssignment4Service);
    }
}
