package com.example.springdemo.controller;


import com.example.springdemo.dto.ActivitiesDTO;
import com.example.springdemo.dto.MessageDTO;
import com.example.springdemo.dto.UserLogIn;
import com.example.springdemo.entities.Activities;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.ActivitiesRepository;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.services.ActivitiesService;
import com.example.springdemo.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeoutException;

@CrossOrigin
@RestController
@RequestMapping(value = "/user")
public class LogInController {

    private final UserService userService;
    private final static String QUEUE_NAME = "hello";


    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private final ActivitiesService activitiesService;

    @Autowired
    private ActivitiesRepository activitiesRepository;
    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    public LogInController(UserService userService, ActivitiesService activitiesService) {
        this.userService = userService;
        this.activitiesService = activitiesService;
    }

    public  void myMethod() throws IOException, TimeoutException {

        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            ActivitiesDTO activitiesDTO = objectMapper.readValue(message,ActivitiesDTO.class);
            Activities activities1 = activitiesService.myFunc(activitiesDTO);
            if (activities1 != null) {
                List<Caregiver> caregiverList=  caregiverRepository.findAll();
                for(Caregiver caregiver:caregiverList){
                    List<Patient> patients=caregiver.getPatientList();
                    for(Patient patient:patients){
                        if(patient.getIdpatient().equals(activities1.getPatient().getIdpatient())){
                            System.out.println(caregiver.getIdcaregiver());
                            this.template.convertAndSend("/topic/socket/caregiver/"+caregiver.getIdcaregiver(),new MessageDTO("Patient " + activities1.getPatient().getIdpatient() + " is " + activities1.getActivity() + " too much"));
                            return;
                        }
                    }
                }
            }


               };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }

    @PostMapping(value = "/login")
    public Integer logIn(@RequestBody UserLogIn userLogIn) throws IOException, TimeoutException {
        boolean has_account = userService.login(userLogIn.getUsername(),userLogIn.getPassword());
        if (has_account == true){
            if(userService.patient_doctor_or_caregiver(userLogIn.getUsername(),userLogIn.getPassword()) == 2){
                myMethod();
            }
            return userService.patient_doctor_or_caregiver(userLogIn.getUsername(),userLogIn.getPassword());
        }
        else {
            return 0;
        }
    }



}
